import os
from flask import Flask, jsonify
from flask_cors import CORS
import json
from flask import request
import service_util
from config_reader import ConfigReader
import db_utils as dbutil
import BusinessProcess as bprocess

app = Flask(__name__)
CORS(app)
cf=ConfigReader()
asst_config=cf.getAssistantServiceConfig()
assistant=service_util.getAssistantApi()
discovery=service_util.getDiscoveryApi()

#import logging

#logging.basicConfig(filename='D:\MyWorkspace\GVRBot\db.log')
#logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)


@app.route('/')
def Welcome():
    return app.send_static_file('index.html')

@app.route('/gvrchat', methods = ['POST'])
def GetAssistantResult():
    
    print('..................request.headers.........',request.headers['Content-Type'])
    nodeName=""
    siteId=""
    output_text=""
    errorCode=""
    cityName=""
    streetName=""
    state=""
    location=[]
    password=""
    username=""
    zipcode=""
    brandName=""
    phone=""
    stateName=""
    fuelingPosition=""
    dispNo=""
    siteName=""
    result={}
    #context_resp
    
    if request.headers['Content-Type'] == 'application/json':
        #print('...................request.json.........',request.json)    
        content = request.get_json()
        print("......content.....",content)        
        req_id=content['id']
        req_text=content['text']
        req_context=content['context']
        #print("\n...req_id..: ",req_id,"...req_text..: ",req_text,"...req_context..: ",req_context)
        response = service_util.getAssistantMessage(assistant,req_text,req_context)
         
        # message(self, workspace_id, input=None, alternate_intents=None, context=None, entities=None, intents=None, output=None, nodes_visited_details=None, **kwargs)
    
    resJson= json.dumps(response, indent=2)
    resp = json.loads(resJson)
    if len(resp['output']['text']) >0: 
        output_text=resp['output']['text'][0]
    context_resp=resp['context']
    if 'nodeName' in resp['context'].keys():
        nodeName=resp['context']['nodeName']
        print("nodeName:   ",nodeName)
    if 'siteID' in resp['context'].keys():
         siteId=resp['context']['siteID']
         print("siteID value :   ",siteId)
    

    if nodeName == 'password_entered':
             
        if 'username' in resp['context'].keys():
         username=resp['context']['username']
         print("username value :   ",username)    
         
        if 'password' in resp['context'].keys():
         password =resp['context']['password']
         print("password  value :   ",password )
        
        if username.lower() == "GVRChatbotTeam".lower() and password == "chatbot1234":
            print("valid User:   ",username )
            response = service_util.getAssistantMessage(assistant,"validUser",context_resp)
            resJson= json.dumps(response, indent=2)
            resp = json.loads(resJson)
            context_resp=resp['context']
            output_text=resp['output']['text'][0]
        else:
            print("Invalid User:   ",username )
            response = service_util.getAssistantMessage(assistant,"invalidUser",context_resp)
            resJson= json.dumps(response, indent=2)
            resp = json.loads(resJson)
            context_resp=resp['context']

            output_text=resp['output']['text'][0]
           

    if nodeName == 'justNumber_handler':
        if 'number' in resp['context'].keys():
            number=resp['context']['number']
            print("number value :   ",number)
            if len(number) ==6:
                text= "Site "+  number              
                response = service_util.getAssistantMessage(assistant,text,context_resp)
                resJson= json.dumps(response, indent=2)
                resp = json.loads(resJson)
                context_resp=resp['context']
                if 'nodeName' in resp['context'].keys():
                    nodeName=resp['context']['nodeName']
                    print("nodeName:   ",nodeName)
                if 'siteID' in resp['context'].keys():
                    siteId=resp['context']['siteID']
                    print("siteID value :   ",siteId)
            elif len(number) < 6:   # output_text=resp['output']['text'][0]           
                text= "Error "+  number              
                response = service_util.getAssistantMessage(assistant,text,context_resp)
                resJson= json.dumps(response, indent=2)
                resp = json.loads(resJson)
                context_resp=resp['context']
                if 'nodeName' in resp['context'].keys():
                    nodeName=resp['context']['nodeName']
                    print("nodeName:   ",nodeName)
                errorCode=number
                      
   
  
    if nodeName == 'cityName_entered' or nodeName == 'streetName_entered' or nodeName == 'location_entered' or nodeName =='state_entered' or nodeName =='brand_entered' or nodeName =='zipcode_entered' or nodeName =='brandLocation_entered' or nodeName =='stateBrandLocation_entered' or nodeName =='stateLocation_entered' or nodeName =='siteName_entered' :
        if 'cityName' in resp['context'].keys(): ###
         cityName=resp['context']['cityName']
         print("city_name value :   ",cityName)
        
        if 'streetName' in resp['context'].keys(): ###
         streetName=resp['context']['streetName']
         print("streetName value :   ",streetName)
        
        if 'state' in resp['context'].keys(): ###
         state=resp['context']['state']
         print("state value :   ",state,"type (state) : ", type(state))
        
        if 'location' in resp['context'].keys():
            location=resp['context']['location']
            print("location value :   ",location)
            
        if 'brand' in resp['context'].keys():
            brandName=resp['context']['brand'] 
            print("brand value :   ",brandName)
        
        if 'zipcode' in resp['context'].keys():
            zipcode=resp['context']['zipcode']
            print("zipcode value :   ",zipcode)
        
        if 'stateName' in resp['context'].keys():
            stateName=resp['context']['stateName']
            print("stateName value :   ",stateName)
        
        if 'siteName' in resp['context'].keys():
            siteName=resp['context']['siteName']
            print("siteName value :   ",siteName)
        
        if nodeName == 'location_entered' or nodeName == 'brandLocation_entered' or nodeName == 'stateBrandLocation_entered' or nodeName == 'stateLocation_entered'  :
            
            print("location value :   ",location, "type (location) : ", type(location))
            location1= [i for i in location ]#location
            state1=[i for i in state ]
            if len (location) >0:
                         
                if stateName and len(stateName) !=2 :
                    if stateName in location:
                        location1.remove(stateName)
                        print("Removing from location stateName:  ",stateName)
                if len(stateName) ==2:
                    for sta in state:
                       print("len(state)111   ",len(state))
                      # print("sta: ",sta) 
                       if sta in location:
                            location1.remove(sta)
                            #print("Removing from location:  ",sta)
                       else:
                            state1.remove(sta)
                           # print("Removing from State:  ",sta)
                for loc in location:
                    if loc.lower() == "indian":
                        location1.remove(loc)
                        state1.append("IN")
                        
                cityName= location1    
                streetName=location1
                state=state1
                count,siteid,dbText,siteListUi= bprocess.getSiteDetailsSearchBy(state,cityName,streetName,brandName,zipcode,phone,siteName,True)
            else:
                count,siteid,dbText,siteListUi= bprocess.getSiteDetailsSearchBy(state,cityName,streetName,brandName,zipcode,phone,siteName,False)
        else:
          count,siteid,dbText,siteListUi= bprocess.getSiteDetailsSearchBy(state,cityName,streetName,brandName,zipcode,phone,siteName,False)  
        r=range(2,25)
               
        if count == 0 :
             #output_text=bprocess.processForCitySearch(cityName)
             print('------------SiteNotFound---------count:  ',count)            
             response = service_util.getAssistantMessage(assistant,'SiteNotFound',context_resp)
             #print('Response for City Name Not Found  from conversation: ',response)
             resJson= json.dumps(response, indent=2)
             resp = json.loads(resJson)
             output_text=resp['output']['text'][0]
             context_resp=resp['context']
        elif count  ==1:
             print('------------oneSiteFound--------count:  ',count, "siteid: ",siteid)   
             #output_text=bprocess.processForOneCity(cityName)
             text1= "Site "+ str(siteid)
             response = service_util.getAssistantMessage(assistant,text1,context_resp)
             #print('Response for City Name Not Found  from conversation: ',response)
             resJson= json.dumps(response, indent=2)
             resp = json.loads(resJson)
             output_text=resp['output']['text'][0]
             context_resp=resp['context']
             if 'nodeName' in resp['context'].keys():
                 nodeName=resp['context']['nodeName']
                 print("nodeName:   ",nodeName)
             if 'siteID' in resp['context'].keys():
                 siteId=resp['context']['siteID']
                 print("siteID value :   ",siteId)
        elif count in r :
             print('------------moreSitesFound--------count:  ',count)   
             #output_text=bprocess.processForOneCity(cityName)
             result={'type':'list','title':'Site Search','data':siteListUi}
             response = service_util.getAssistantMessage(assistant,'moreSitesFound',context_resp)
             #print('Response for City Name Not Found  from conversation: ',response)
             resJson= json.dumps(response, indent=2)
             resp = json.loads(resJson)
           
             output_text=dbText
             context_resp=resp['context']
        
        else: #>15
             print('------------tooManySitesFound--------count:  ',count) 
             response = service_util.getAssistantMessage(assistant,'tooManySitesFound',context_resp) #cityNameNotFound
             #print('Response for City Name Not Found  from conversation: ',response)
             resJson= json.dumps(response, indent=2)
             resp = json.loads(resJson)
             
             context_resp=resp['context']
             asstResponseText=resp['output']['text'][0]
             print('------------dbText---:  ',dbText, "........asstResponseText: ",asstResponseText) 
             output_text=dbText+asstResponseText
             
    if nodeName == 'siteID_entered':    
        print(" inside nodeName......... :   ",nodeName)        
        if dbutil.isValidSiteId(siteId):
           output_text= bprocess.processForValidSiteId(siteId)
           response = service_util.getAssistantMessage(assistant,'validSiteID',context_resp)
           resJson= json.dumps(response, indent=2)
           resp = json.loads(resJson)
           context_resp=resp['context']
           # text= "Valid Siteid"
        else:
            #text = "We are not able to find the given SideID "+ siteId+ ". Please enter city name to search Site by location."
            response = service_util.getAssistantMessage(assistant,'siteIDnotFound',context_resp)
            resJson= json.dumps(response, indent=2)
            resp = json.loads(resJson)
            #output_text=resp['output']['text'][0]
            sitesimilar, sitetable= bprocess.getSimilarSiteList(siteId)
            output_text=sitesimilar
            context_resp=resp['context']
    if nodeName == 'fuelingPosition_entered':
        print(" inside nodeName......... :   ",nodeName)         
        if 'siteID' in resp['context'].keys(): ###
            siteid=resp['context']['siteID']
            print("siteid value :   ",siteid)
            
        if 'fuelingPosition' in resp['context'].keys():
            fuelingPosition=resp['context']['fuelingPosition']
            print("fuelingPosition value :   ",fuelingPosition)
        
        if dbutil.isValidSiteId(siteId):
            print("Valid Site ID :   ",siteId)
            if dbutil.isValidFuelingPos(siteId,fuelingPosition):
                print("Valid fuelingPosition :   ",fuelingPosition)
                recordCount,isOffline,text= bprocess.processForfuelingPosition(siteId,fuelingPosition)
                print("recordCount :   ",recordCount, " Text: ",text)                
                if recordCount==0:
                    response = service_util.getAssistantMessage(assistant,'noIssuesFound',context_resp)
                    #print('Response for City Name Not Found  from conversation: ',response)
                    resJson= json.dumps(response, indent=2)
                    resp = json.loads(resJson)
                    output_text=resp['output']['text'][0]
                    context_resp=resp['context'] 
                else:
                    response = service_util.getAssistantMessage(assistant,'issuesFound',context_resp)
                    #print('Response for City Name Not Found  from conversation: ',response)
                    response['context']['isOffline']=isOffline                  
                    resJson= json.dumps(response, indent=2)
                    resp = json.loads(resJson)
                    #output_text=resp['output']['text'][0] +text
                    output_text=text
                    context_resp=resp['context'] 
            else:
                print("InValid fuelingPosition :   ",fuelingPosition)                
                response = service_util.getAssistantMessage(assistant,'pumpNotFound',context_resp)
                #print('Response for City Name Not Found  from conversation: ',response)
                resJson= json.dumps(response, indent=2)
                resp = json.loads(resJson)
                output_text=resp['output']['text'][0]
                context_resp=resp['context'] 

        else:
          print("Invalid Site ID :   ",siteId)
          response = service_util.getAssistantMessage(assistant,'siteIDnotFound',context_resp)
          #print('Response for City Name Not Found  from conversation: ',response)
          resJson= json.dumps(response, indent=2)
          resp = json.loads(resJson)
          #output_text=resp['output']['text'][0]
          output_text="Siteid "+ siteId+" is invalid. Please check the siteid and try again" 
          context_resp=resp['context']  
         
    if nodeName == 'dispNo_entered':
        print(" inside nodeName......... :   ",nodeName)         
        if 'siteID' in resp['context'].keys(): ###
            siteid=resp['context']['siteID']
            print("siteid value :   ",siteid)
            
        if 'dispNo' in resp['context'].keys():
            dispNo=resp['context']['dispNo']
            print("dispNo value :   ",dispNo)
        
        if dbutil.isValidSiteId(siteId):
            print("Valid Site ID :   ",siteId)
            if dbutil.isValidDispenserPos(siteId,dispNo):
                print("Valid dispNo :   ",dispNo)
                recordCount,isOffline,text= bprocess.processForDispNumber(siteId,dispNo)
                print("recordCount :   ",recordCount, " Text: ",text)                
                if recordCount==0:
                    response = service_util.getAssistantMessage(assistant,'noIssuesFound',context_resp)
                    #print('Response for City Name Not Found  from conversation: ',response)
                    resJson= json.dumps(response, indent=2)
                    resp = json.loads(resJson)
                    output_text=resp['output']['text'][0]
                    context_resp=resp['context'] 
                else:
                    response = service_util.getAssistantMessage(assistant,'issuesFound',context_resp)
                    #print('Response for City Name Not Found  from conversation: ',response)
                    resJson= json.dumps(response, indent=2)
                    resp = json.loads(resJson)
                    #output_text=resp['output']['text'][0] +text
                    output_text=text
                    context_resp=resp['context'] 
            else:
                print("InValid dispNo :   ",dispNo)                
                response = service_util.getAssistantMessage(assistant,'dispenserNotFound',context_resp)
                #print('Response for City Name Not Found  from conversation: ',response)
                resJson= json.dumps(response, indent=2)
                resp = json.loads(resJson)
                output_text=resp['output']['text'][0]
                context_resp=resp['context'] 

        else:
          print("Invalid Site ID :   ",siteId)
          response = service_util.getAssistantMessage(assistant,'siteIDnotFound',context_resp)
          #print('Response for City Name Not Found  from conversation: ',response)
          resJson= json.dumps(response, indent=2)
          resp = json.loads(resJson)
          #output_text=resp['output']['text'][0]
          output_text="Siteid "+ siteId+" is invalid. Please check the siteid and try again" 
          context_resp=resp['context']        
    
    if nodeName == 'siteWideSearch':
        if 'siteID' in resp['context'].keys(): ###
            siteid=resp['context']['siteID']
            print("siteid value :   ",siteid)
        
        if 'isOffline' in resp['context'].keys(): ###
            isOffline=resp['context']['isOffline']
            print("isOffline value :   ",isOffline)
        if not isOffline :
            response = service_util.getAssistantMessage(assistant,'noCommunicationError',context_resp)
            #print('Response for City Name Not Found  from conversation: ',response)
            resJson= json.dumps(response, indent=2)
            resp = json.loads(resJson)
            output_text=resp['output']['text'][0]
            context_resp=resp['context']    
        else :
            status,text=bprocess.findOtherOfflineFuelData(siteid)
            if status :
                response = service_util.getAssistantMessage(assistant,'sameIssueNotFound',context_resp)
                #print('Response for City Name Not Found  from conversation: ',response)
                resJson= json.dumps(response, indent=2)
                resp = json.loads(resJson)
                #output_text=resp['output']['text'][0]
                output_text=text
                context_resp=resp['context']
            else :
                response = service_util.getAssistantMessage(assistant,'sameIssueFound',context_resp)
                #print('Response for City Name Not Found  from conversation: ',response)
                resJson= json.dumps(response, indent=2)
                resp = json.loads(resJson)
                #output_text=resp['output']['text'][0]
                output_text=text
                context_resp=resp['context']
    
    if nodeName == 'deviceID_entered':
        if 'siteID' in resp['context'].keys(): ###
            siteid=resp['context']['siteID']
            print("siteid value :   ",siteid)
        if 'deviceID' in resp['context'].keys(): ###
             deviceid=resp['context']['deviceID']
             print("deviceid value :   ",deviceid)
        output_text=bprocess.findKpiAndStateOpt1(siteid,deviceid)
    if nodeName == 'dispDeviceID_entered': 
        if 'siteID' in resp['context'].keys(): ###
            siteid=resp['context']['siteID']
            print("siteid value :   ",siteid)
        if 'dispDeviceID' in resp['context'].keys(): ###
             dispDeviceid=resp['context']['dispDeviceID']
             print("dispDeviceID value :   ",dispDeviceid)
        output_text=bprocess.findKpiAndStateOpt2(siteid,dispDeviceid)
    if nodeName == 'dispPosition_entered':
        if 'siteID' in resp['context'].keys(): ###
            siteid=resp['context']['siteID']
            print("siteid value :   ",siteid)
        if 'dispPosition' in resp['context'].keys(): ###
             dispPosition=resp['context']['dispPosition']
             print("dispPosition value :   ",dispPosition)
        output_text=bprocess.findKpiAndStateOpt3(siteid,dispPosition)
    
    if nodeName == 'troubleshoot_error_code':
        if 'errorCode' in resp['context'].keys(): ###
            errorCode=resp['context']['errorCode']
            print("errorCode value :   ",errorCode)
        
        output_text=service_util.executeTroubleshootGauid(discovery,errorCode)
    
    outPut = {'id': req_id,'text':output_text , 'context':context_resp,'result':result}
    outPut_json=jsonify(outPut) 
    print('...................outPut_json........',outPut_json)
    #return(json.dumps(response, indent=2))
    return outPut_json


port = os.getenv('PORT', '5000')
if __name__ == "__main__":
	app.run(host='0.0.0.0', port=int(port))# -*- coding: utf-8 -*-

