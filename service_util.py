from watson_developer_cloud import AssistantV1
from config_reader import ConfigReader 
from watson_developer_cloud import DiscoveryV1
import json

cf=ConfigReader()
asst_config=cf.getAssistantServiceConfig()
dis_config=cf.getDiscoveryServiceConfig()
def getAssistantApi():
    assistant = AssistantV1(
        version=asst_config['version'],
        username=asst_config['username'],
        password=asst_config['passwd'],
        url=asst_config['url']
    )
    return assistant
    
def getAssistantMessage(assistant,req_text,req_context):
   
    print(" Input to Assistant Service:  Input Text",req_text, "\n...req_context..: ",req_context)
    response = assistant.message(
            workspace_id=asst_config['workspace_id'],
            input={
            'text': req_text
              },
              
              
            context = req_context
         )
   
    #print('Response from Assistant',response.encode("utf-8"))     
    return response


def getDiscoveryApi():
    discovery = DiscoveryV1(
        version=dis_config['version'],
        username=dis_config['username'],
        password=dis_config['passwd'],
        url=dis_config['url']
    )
    return discovery
    
def executeTroubleshootGauid(discovery,errorCode):
     
    r=range(1,11)
    if int(errorCode) in r or int(errorCode) ==12:
        errorCode="E"+str(errorCode)
        print ("errorCode  :",errorCode)
    filtertxt='error_code::'+errorCode
    print ('filtertxt: ',filtertxt)    
    my_query = discovery.query(dis_config['tshoot_environment_Id'],dis_config['tshoot_collection_id'],
                               filter=filtertxt)

    resJson= json.dumps(my_query, indent=2)
    resp = json.loads(resJson)
    print("resp: ",resp)
   # print("dfvdkfj: ",resp['results'][0])
    resultCount=len(resp['results'])
    print("resultCount: ",resultCount)    
    if resultCount==0 :
        return "There is no error code matching. Please enter a valid error code."
    
    description=resp['results'][0]['description']
    solution=resp['results'][0]['solution:']
    text="The error {} indicates {}. {}".format(errorCode,description,solution)    
    print(" text: ",text)
    return text
    
 
 
 
#print(dis_config['tshoot_collection_id'])
    
#print(executeTroubleshootGauid(getDiscoveryApi(),'9')) 