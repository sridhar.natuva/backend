# -*- coding: utf-8 -*-
import yaml
with open("db_queries.yml", 'r') as ymlfile:
    cfg = yaml.load(ymlfile)

class DbQueryReader:

    def __init__(self):

        with open("db_queries.yml", 'r') as f:
         self.cfg = yaml.safe_load(f)
         # redshift parameters
         self.redshift_quries = cfg['redshift_quries']
        
         
    def getRedshiftQuries(self):
        return self.redshift_quries
      