# -*- coding: utf-8 -*-
import sqlalchemy as sa
from sqlalchemy.orm import sessionmaker
from config_reader import ConfigReader
from dbquery_reader import DbQueryReader

cf=ConfigReader()
dbcon=cf.getDbConfig()
db_query=DbQueryReader()

def getDbSession():
    DATABASE = dbcon['database']
    USER = dbcon['user']
    PASSWORD = dbcon['passwd']
    HOST = dbcon['host']
    PORT = dbcon['port']
    SCHEMA = dbcon['schema']
    connection_string = "redshift+psycopg2://%s:%s@%s:%s/%s" % (USER,PASSWORD,HOST,str(PORT),DATABASE)
    try:   
        engine = sa.create_engine(connection_string)
        session = sessionmaker()
        session.configure(bind=engine)
        s = session()
        SetPath = "SET search_path TO %s" % SCHEMA
        s.execute(SetPath)
        return s
    except Exception as error:
         print("Something bad happened: %s", error)
         s.close()

def executeQuery(query,param):
    dbSession=getDbSession()
    #query = "SELECT site_id, kpi  FROM dispenser_kpi where site_id=?;"
    #rr = dbSession.execute(query,{'id':195452})
    rr = dbSession.execute(query,param)
    all_results = rr.fetchall()
    #print(all_results)
    dbSession.close()
    return all_results


def executeQueryWithoutParam(query):
    dbSession=getDbSession()
    #query = "SELECT site_id, kpi  FROM dispenser_kpi where site_id=?;"
    #rr = dbSession.execute(query,{'id':195452})
    rr = dbSession.execute(sa.text(query))
    all_results = rr.fetchall()
    #print(all_results)
    dbSession.close()
    return all_results
    
#select count(*) from public.site where site_id = 195452

def isValidSiteId(site_id):
    isValid=False    
    siteid_query=db_query.getRedshiftQuries()['q_siteid']
    param={'siteid':site_id}
    result= executeQuery(siteid_query,param)
    print("..result..",result[0][0])
    if result[0][0] !=0:
        isValid=True
    return isValid

def getSiteIdDetails(site_id):
       
    siteid_query=db_query.getRedshiftQuries()['q_siteid_details']
    param={'siteid':site_id}
    result= executeQuery(siteid_query,param)
    print("..result..",result)
    return result
 
def getSiteIssueDetails(site_id,dispenser_position,fueling_position):
       
    siteid_query=db_query.getRedshiftQuries()['q_site_search1']
    param={'site_id':site_id,'dispenser_position':dispenser_position,'fueling_position':fueling_position}
    result= executeQuery(siteid_query,param)
    print("..result..",result)
    return result    


#getSiteIssueDetails(245050,'5-6',5)

def getDeviceList(siteId):
      
    siteid_query=db_query.getRedshiftQuries()['q_device_id']
    param={'siteid':siteId}
    result= executeQuery(siteid_query,param)
    #all_results = result.fetchall()
    print("..result..",result)
    return result  
    
def getErrorDeviceList(siteId):
    
    siteid_query=db_query.getRedshiftQuries()['q_site_search']
    param={'site_id':siteId}
    result= executeQuery(siteid_query,param)
    #all_results = result.fetchall()
    print("..result..",result)
    return result 

def getSimilarSiteid(siteId):
    
    siteid_query=db_query.getRedshiftQuries()['q_site_similar']
    siteId="%"+siteId+"%"
    print("siteId: ",siteId)    
    param={'siteid':siteId}
    result= executeQuery(siteid_query,param)
    #all_results = result.fetchall()
    print("..result..",result)
    return result 
    
def getCityCount(city):
    siteid_query=db_query.getRedshiftQuries()['q_city']
    param={'city':city}
    result= executeQuery(siteid_query,param)
    print("..result..",result)
    i=len(result)
    return i

def getSiteListByCity(city_name):
    
    city_query=db_query.getRedshiftQuries()['q_city_search']
    param={'city':city_name}
    result= executeQuery(city_query,param)
    #all_results = result.fetchall()
    print("..result..",result)
    return result 
    
    
def getSiteListByCityStreetName(city_name, street_name):
    
    city_query=db_query.getRedshiftQuries()['q_city_street_search']
    param={'city':city_name,'address_line':street_name}
    result= executeQuery(city_query,param)
    #all_results = result.fetchall()
    print("..result..",result)
    return result     

def getDispenserIssueOpt1(siteid,deviceid):
    
    city_query=db_query.getRedshiftQuries()['q_dispenser_l2_opt1']
    param={'siteid':siteid,'deviceid':deviceid}
    result= executeQuery(city_query,param)
    #all_results = result.fetchall()
    print("..result..",result)
    return result 


def getDispenserIssueOpt2(siteid,dispDeviceid):
    
    city_query=db_query.getRedshiftQuries()['q_dispenser_l2_opt2']
    param={'siteid':siteid,'dispenserDeviceId':dispDeviceid}
    result= executeQuery(city_query,param)
    #all_results = result.fetchall()
    print("..result..",result)
    return result 

def getDispenserIssueOpt3(siteid,dispenser_position):
    
    city_query=db_query.getRedshiftQuries()['q_dispenser_l2_opt3']
    param={'siteid':siteid,'dispenserPosition':dispenser_position}
    result= executeQuery(city_query,param)
    #all_results = result.fetchall()
    print("..result..",result)
    return result 


def getDispenserIssueOpt33(siteid,dispenserDeviceId):
    
    city_query=db_query.getRedshiftQuries()['q_dispenser_l2_opt33']
    param={'siteid':siteid,'dispenserDeviceId':dispenserDeviceId}
    result= executeQuery(city_query,param)
    #all_results = result.fetchall()
    print("..result..",result)
    return result 

def getDispenserIssueL3(fueling_position,device_id):
    
    city_query=db_query.getRedshiftQuries()['q_dispenser_l3']
    param={'deviceid':device_id}
    result= executeQuery(city_query,param)
    #all_results = result.fetchall()
    print("..result..",result)
    return result 

def getDispenserIssueBySiteIDL2(siteid):
    
    city_query=db_query.getRedshiftQuries()['q_dispenser_m5m7_l2']
    param={'siteid':siteid}
    result= executeQuery(city_query,param)
    #all_results = result.fetchall()
    print("..result..",result)
    return result 

def getDispenserIssueBySiteIDeviceidKpiState(siteid,deviceid,kpi,state):
    
    city_query=db_query.getRedshiftQuries()['q_dispenser_issue']
    param={'siteid':siteid,'deviceid':deviceid,'kpi':kpi,'state':state}
    result= executeQuery(city_query,param)
    #all_results = result.fetchall()
    print("..result..",result)
    return result 

def getIncidentDetailsBySiteIDeviceidKpiState(siteid,deviceid,kpi,state):
    
    city_query=db_query.getRedshiftQuries()['q_incidents_details']
    param={'siteid':siteid,'deviceid':deviceid,'kpi':kpi,'state':state}
    result= executeQuery(city_query,param)
    #all_results = result.fetchall()
    print("..result..",result)
    return result   

def getStreetCount(street_name):
    siteid_query=db_query.getRedshiftQuries()['q_street']
    param={'street_name':street_name}
    result= executeQuery(siteid_query,param)
    print("..result..",result)
    i=len(result)
    return i


def getSitesSearchBy(state_name,city_name, street_name,brand_name,postal_code,phone,site_name):
    
    city_query=db_query.getRedshiftQuries()['q_site_search_by1']
    #--LIKE NVL(lower('%NC%'),lower(s.state)) -- lower(s.state) in (NVL(lower(NULL),lower(s.state)))  --NVL(lower(NULL),lower(s.state)),
    print("state_name: ",state_name, " city_name :",city_name )
    
    print("street_name: ",street_name, " brand_name :",brand_name )
    
    print("postal_code: ",postal_code, " phone :",phone )
    
    print("site_name: ",site_name)
    if not state_name:
        state_name="((NULL) IS NULL OR lower(s.state) IN (NULL))"
    else:
        state_name=" (("+state_name+") IS NULL OR lower(s.state) IN ("+state_name+"))"        
    city_query = city_query+state_name
    
    if not city_name:
        city_name=" AND   lower(s.city) LIKE NVL(lower(NULL),lower(s.city))"
    else:
        city_name=" AND   lower(s.city) LIKE NVL(lower('%%"+city_name+"%%'),lower(s.city))"
    city_query = city_query+city_name
    
    if not street_name:
        street_name=" AND   lower(s.address_line_1) LIKE NVL(lower(NULL),lower(s.address_line_1))"
    else:
        street_name=" AND   lower(s.address_line_1) LIKE NVL(lower('%%"+street_name+"%%'),lower(s.address_line_1))"
    city_query = city_query+street_name
    
    if not brand_name:
        brand_name=" AND   lower(s.brand) LIKE NVL(lower(NULL),lower(s.brand))"
    else:
        brand_name= " AND   lower(s.brand) LIKE NVL(lower('%%"+brand_name+"%%'),lower(s.brand))"
    city_query = city_query+brand_name    
    
    if not postal_code:
        postal_code= " AND   lower(s.postal_code) LIKE NVL(lower(NULL),lower(s.postal_code))"
    else:
        postal_code=" AND   lower(s.postal_code) LIKE NVL(lower('"+postal_code+"'),lower(s.postal_code))"
    city_query = city_query+postal_code    
    
    if not phone:
        phone= " AND   lower(s.phone) LIKE NVL(lower(NULL),lower(s.phone))"
    else:
        phone= "AND   lower(s.phone) LIKE NVL(lower('"+phone+"'),lower(s.phone))"
    city_query = city_query+phone 
    
    if not site_name:
        site_name="AND   lower(s.site_name) LIKE NVL(lower(NULL),lower(s.site_name))"
    else:
        site_name=" AND   lower(s.site_name) LIKE NVL(lower('%%"+site_name+"%%'),lower(s.site_name))"
        
    city_query = city_query+site_name
    
    city_query =city_query+db_query.getRedshiftQuries()['q_site_search_by2']
    
    
    #param={'state':state_name,'city':city_name,'address_line_1':street_name,'brand':brand_name,'postal_code':postal_code,'phone':phone,'site_name':site_name}
    
    print("city_query : ",city_query )    
    result= executeQueryWithoutParam(city_query)
    #all_results = result.fetchall()
    print("..result Count: ",len(result))    
    print("..result..",result)
    return result 

def getSitesSearchByCityState(state_name,city_name, street_name,brand_name):
    
    q_site_search_by_city_state=db_query.getRedshiftQuries()['q_site_search_by_city_state1']
   
    print("state_name: ",state_name, " city_name :",city_name )
    
    print("street_name: ",street_name, "brand_name: ",brand_name, )
    
    if not state_name:
        state_name1="((NULL) IS NULL OR lower(s.state) IN (NULL))"
    else:
        state_name1=" (("+state_name+") IS NULL OR lower(s.state) IN ("+state_name+"))"        
    
    q_site_search_by_city_state = q_site_search_by_city_state+state_name1
           
    if not brand_name:
        brand_name1=" AND   lower(s.brand) LIKE NVL(lower(NULL),lower(s.brand))"  
    else:
        brand_name1= " AND   lower(s.brand) LIKE NVL(lower('%%"+brand_name+"%%'),lower(s.brand))"
        
    q_site_search_by_city_state = q_site_search_by_city_state+brand_name1
    
    if not city_name:
        city_name1=" AND  (((NULL) IS NULL OR lower(s.city) IN (NULL))"  
    else:
        city_name1="AND  ((("+city_name+") IS NULL OR lower(s.city) IN ("+city_name+"))" 
    
    q_site_search_by_city_state = q_site_search_by_city_state+city_name1
        
    if not street_name:
        street_name1=" or ((NULL) IS NULL OR lower(s.address_line_1) IN (NULL)))"
    else:
        
         street_name1=" or (("+street_name+") IS NULL OR lower(s.address_line_1) IN ("+street_name+")))"   
           
    q_site_search_by_city_state = q_site_search_by_city_state+street_name1
    
    q_site_search_by_city_state =q_site_search_by_city_state+db_query.getRedshiftQuries()['q_site_search_by_city_state2']
    
    #param={'state':state_name,'brand':brand_name,'city':city_name,'address_line_1':street_name}
    print("q_site_search_by_city_state :",q_site_search_by_city_state)
    
    
    result= executeQueryWithoutParam(q_site_search_by_city_state)
    #all_results = result.fetchall()
    print("..result Count: ",len(result))    
    print("..result..",result)
    return result 

def getFuelingPositionDetails(siteid,fueling_position):
    print("siteid: ",siteid, " fueling_position :",fueling_position )
    query=db_query.getRedshiftQuries()['q_search_by_fueling_position']
    param={'site_id':siteid,'fueling_position':fueling_position}
    result= executeQuery(query,param)
    #all_results = result.fetchall()
    print("..result..",result)
    return result   

#getFuelingPositionDetails(155881,5)

def getDispDeviceId(siteid,dispenser_position):
    print("siteid: ",siteid, " dispenser_position :",dispenser_position )
    query=db_query.getRedshiftQuries()['q_search_by_disp_device_id']
    param={'site_id':siteid,'dispenser_position':dispenser_position}
    result= executeQuery(query,param)
    #all_results = result.fetchall()
    print("..result..",result)
    return result  

#getDispDeviceId(155881,'2-2')

def getDispDetails(siteid,dispenser_device_id):
    print("siteid: ",siteid, " dispenser_device_id :",dispenser_device_id )
    query=db_query.getRedshiftQuries()['q_search_by_disp_number']
    param={'site_id':siteid,'dispenser_device_id':dispenser_device_id}
    result= executeQuery(query,param)
    #all_results = result.fetchall()
    print("..result..",result)
    return result 

#getDispDetails(155881,2923878)

def isValidFuelingPos(site_id,fuelingPos):
    isValid=False    
    siteid_query=db_query.getRedshiftQuries()['q_fueling_pos_validation']
    param={'site_id':site_id,'fueling_position':fuelingPos}
    result= executeQuery(siteid_query,param)
    print("..result..",result[0][0])
    if result[0][0] !=0:
        isValid=True
    return isValid

def isValidDispenserPos(site_id,disPos):
    isValid=False    
    siteid_query=db_query.getRedshiftQuries()['q_dispenser_pos_validation']
    param={'site_id':site_id,'dispenser_position':disPos}
    result= executeQuery(siteid_query,param)
    print("..result..",result[0][0])
    if result[0][0] !=0:
        isValid=True
    return isValid

def getPosOffline(siteid):
    print("siteid: ",siteid )
    query=db_query.getRedshiftQuries()['q_fueling_pos_offline']
    param={'site_id':siteid}
    result= executeQuery(query,param)
    #all_results = result.fetchall()
    print("..result..",result)
    return result  
#getPosOffline (155881) 







#getSiteIdDetails(846744)

#getSimilarSiteid('5068')

#print(getDispenserIssueOpt1(195452,12800))

#print(getStreetCount('2600 RANDLEMAN ROAD')) #2600 RANDLEMAN ROAD'

#print(getSiteListByCityStreetName('GREENSBORO', '2600 RANDLEMAN ROAD'))

#print(getSiteListByCityStreetName('GREENSBORO', '2600 RANDLEMAN ROAD'))
      
#print(getSiteListByCity('GREENSBORO'))    
 
#print(isValidSiteId(195452))   
 
 #print(isValidCity('BELEN'))
 
 #print(isValidCity('BExxxLEN'))
  
 #print(getDeviceList(195452)) 
  
#print(getErrorDeviceList(165600)) 
  
#getSitesSearchByCityState('\'nc\',\'in\'', '\'greensboro\'','','Gilbar')

#state_name,city_name, street_name,brand_name,postal_code,phone,site_name
#'\'nc\',\'in\''
#getSitesSearchBy('\'nc\',\'in\'','GREENSBORO','','Gilbar','27453','3365470001','')

#getFuelingPositionDetails(825999,1)

#getDispNumberDetails(116482,'3-4')

#print(isValidFuelingPos(116482,8))

#print(isValidDispenserPos(116482,'3-4'))

#getPosOffline(116482)