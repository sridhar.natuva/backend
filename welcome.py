# Copyright 2015 IBM Corp. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#Prabhat Santi
import os
from flask import Flask, jsonify
from flask_cors import CORS
import sqlalchemy as sa
from sqlalchemy.orm import sessionmaker
from watson_developer_cloud import AssistantV1
import json
from flask import request

app = Flask(__name__)
CORS(app)
#cors = CORS(app, resources={r"/api/*": {"origins": "*"}})

DATABASE = "dispensers"
USER = "analytics_ds_dev"
PASSWORD = "LetMeIn1!"
HOST = "innovation-analytics-us-east-1-dev.cwv0hzs5cket.us-east-1.redshift.amazonaws.com"
PORT = "5439"
SCHEMA = "public"  
connection_string = "redshift+psycopg2://%s:%s@%s:%s/%s" % (USER,PASSWORD,HOST,str(PORT),DATABASE)

@app.route('/')
def Welcome():
    return app.send_static_file('index.html')

@app.route('/redshift/')
def WelcomeToMyapp():
    try:   
        engine = sa.create_engine(connection_string)
        session = sessionmaker()
        session.configure(bind=engine)
        s = session()
        SetPath = "SET search_path TO %s" % SCHEMA
        s.execute(SetPath)
        query = "SELECT site_id, kpi  FROM dispenser_kpi limit 20;"
        rr = s.execute(query)
        all_results = rr.fetchall()
        dicList=[]
        for a, b in all_results:
            dic={'site_id':a,
                 'kpi':b}
            dicList.append(dic)
        s.close()
        return jsonify(results=dicList)
    except Exception as error:
         print("Something bad happened: %s", error)
    


@app.route('/api/assistant')
def GetResult():
    assistant = AssistantV1(
        version='2018-06-22',
        username='2756b25b-4806-48a6-965a-57efb6c1c202',
        password='iPAbx47l5eyr',
        url='https://gateway.watsonplatform.net/assistant/api'
    )
    response = assistant.message(
        workspace_id='9138ee75-40ed-4590-9bd1-786e87eef541',
        input={
            'text': 'thank you'
        }
    )
    print('Response from Assistant',response)
    return(json.dumps(response, indent=2))
    #return response

@app.route('/gvrchat', methods = ['POST'])
def GetAssistantResult():
    print('...................Executing..GetAssistantResult.........')    
    assistant = AssistantV1(
        version='2018-06-22',
        username='2756b25b-4806-48a6-965a-57efb6c1c202',
        password='iPAbx47l5eyr',
        url='https://gateway.watsonplatform.net/assistant/api'
    )
    print('..................request.headers.........',request.headers['Content-Type'])
    if request.headers['Content-Type'] == 'application/json':
        print('...................request.json.........',request.json)    
        content = request.get_json()
        print("......content.....",content)        
        print("............content.id....",content['id'])
        print("............content.text....",content['text'])
        id=content['id']
        context=content['context']
        print("............context....",content['context'])
        response = assistant.message(
            workspace_id='9138ee75-40ed-4590-9bd1-786e87eef541',
            input={
            'text': content['text']
            
            
            },
            context = content['context']
         )
         
        # message(self, workspace_id, input=None, alternate_intents=None, context=None, entities=None, intents=None, output=None, nodes_visited_details=None, **kwargs)
    print('Response from Assistant',response)
    resJson= json.dumps(response, indent=2)
    resp = json.loads(resJson)
    text=resp['output']['text'][0]
    context=resp['context']
    outPut = {'id': id,'text':text , 'context':context}
    outPut_json=jsonify(outPut)
    print('...................outPut_json........',outPut_json)
    #return(json.dumps(response, indent=2))
    return outPut_json


port = os.getenv('PORT', '5000')
if __name__ == "__main__":
	app.run(host='0.0.0.0', port=int(port))