# -*- coding: utf-8 -*-

import db_utils as dbutil
from itertools import groupby
from operator import itemgetter


def processForValidSiteId(siteId):
    
    errorDeviceList=dbutil.getErrorDeviceList(siteId)
    siteIdDtls=dbutil.getSiteIdDetails(siteId)
    print("siteIdDtls :",siteIdDtls)  
    if(len(errorDeviceList) == 0):
      text= " No Issue at {} ({}), {} station located in {}, {}, phone: {}\n \n <br>".format(siteIdDtls[0][4],siteId,siteIdDtls[0][0],siteIdDtls[0][1],siteIdDtls[0][2],siteIdDtls[0][3])
      # text2="No issues found in the site: {}".format(siteId)
      return text
    
    errorDeviceList.sort(key = itemgetter(1,4,5))
    groups = groupby(errorDeviceList, itemgetter(1,4,5))
    outputText=[]
    text="Current Issues at {} ({}), {} station located in {}, {}, phone: {}\n \n <br><br>" .format(siteIdDtls[0][4],siteId,siteIdDtls[0][0],siteIdDtls[0][1],siteIdDtls[0][2],siteIdDtls[0][3])
    
    #text= "SiteID {} is a {} station located in {}, {}, phone: {}, is showing error/warning/down status due to:\n \n <br>".format(siteId,siteIdDtls[0][0],siteIdDtls[0][1],siteIdDtls[0][2],siteIdDtls[0][3])
    outputText.append(text)
    for (key, data) in groups:
        dataList= list(data)
       # print("group Data: ",dataList)
        device=dataList[0]
       # print("device: ",device)
        issue_details=dbutil.getSiteIssueDetails(device[0],device[3],device[5])
       # print("issue_details: ",issue_details)
       # text=" <dispenser_type_code> dispenser position <dispenser_position> has the <fueling_position_type> fueling position <fueling_position> has (<device_type_code> decive id <dispenser_device_id> ) as status <status> with a cause of <>
        #text="Dispenser {} {} has device {} is showing {} on fueling position {} due to  {}. <ul>".format(device[2],device[3],device[4],device[8],device[5],issue_details[0][8])
        text1="<br> {} dispenser position {} has the {} fueling position {} has {} decive (id {}) as status {} ".format(device[2] ,device[3] ,device[6],device[5],device[4],device[1],issue_details[0][9])  
        #if issue_details[0][9] == "down":
        issue=str(issue_details[0][8]).strip()
        
        print( "Length :  ",len(issue))
        if  len(issue) >0 :
            
            text1 = text1+ "with a cause of "+issue_details[0][8]
        #device[2] ,device[3] ,device[6],device[5],device[4],device[1],device[8]
        outputText.append(text1)
        for comp in dataList:
           print ("  comp[7] : ", comp[7], "  len(comp[7]  ",len(comp[7])) 
           if comp[7].strip() !='overall-system':
               text_comp="<li>   {} is in {}.</li>".format(comp[7],comp[8])    
               outputText.append(text_comp)
        outputText.append("</ul>")
    text= ''.join(outputText)
    #text1 =text +"</ul>"
    return text

#processForValidSiteId(846744)
 

def getSimilarSiteList(siteId):
   # print("siteId: ",siteId,"len(siteId): ",len(str(siteId))
    siteIdStr=str(siteId)
    siteListTemp=[]
    siteIdTemp=siteIdStr[0:len(siteIdStr)-1]
    siteList=dbutil.getSimilarSiteid(siteIdTemp)
    siteListTable=[]
   # print("siteList  ",siteList)
    i=  len(siteList)  
    text=""    
    if i > 0:
        siteListTable=getSiteListTable(siteList)
        if i>5:
            siteList=siteList[0:5] 
        for site in siteList:
            siteid=site[0]
            # print("siteid: ",siteid, " site :",site )
            siteListTemp.append(str(siteid))
            site_list= ', '.join(siteListTemp)
            #print("site_list :",site_list," siteId: ",siteId )
            text= "Sorry, Can not find any siteID {}. Can you check the number again? But, instead found {} similar siteID(s): {}. Are any of these is the one you are looking for? Please specify !".format(siteId,i,site_list)
    else:
        text= "Sorry, Can not find any siteID {}. Can you check the number again?.<br> Unable to find similar siteIDs as well. You can try searching by location".format(siteId)
    return text,siteListTable
    


def getSiteDetailsSearchBy(state_name,city_name, street_name,brand_name,postal_code,phone,site_name,isCityAndStreet):
    
    #brand_name =""
   # postal_code =""
    stateName=state_name
    cityName=city_name
    streetName=street_name
    if len(state_name) > 0:
        stateName = getStringFromList(state_name)
    else:
        stateName=""
       
        
    #phone=""
    
    if isCityAndStreet:
        if len(city_name) > 0:
            cityName = getStringFromList(city_name)
        else:
            cityName=""
   
        if len(street_name) > 0:
            streetName = getStringFromList(street_name)
        else:
            streetName=""
        siteList=dbutil.getSitesSearchByCityState(stateName,cityName, streetName,brand_name)
    else :
        siteList=dbutil.getSitesSearchBy(stateName,cityName, streetName,brand_name,postal_code,phone,site_name)
    siteListUi=[]
    text1=""
    siteId="";
    i=len(siteList)
    r=range(2,115)
    if i==0:
        text1="No Site Found"
        return i,siteId,text1
    elif i==1:
        siteId=siteList[0][0]
        text1="Only One Site Found"
        return i,siteId,text1
    elif i in r :
        print('Record Count: ',i)
        text1=" I found  {} sites. Here is the list,<ul> ".format(i)
        siteListUi=getSiteSearchList(siteList)
       # outputText.append(info)    
      #  for site in siteList:
           #record="<br>SiteID: {}  Site Name: {} <br> Brand: {}  Phone Number: {} <br> Address: {}, {}, {}, {} <br>".format(site[0],site[1],site[2],site[7],site[3],site[4],site[5],site[6])    
           # outputText.append(record)
           # print('Issue Count: ',site[10])            
           # if site[10]>0:
           #   outputText.append("This site is showing reported issues.<br>")  
           # text= ''.join(outputText)
           # text1 = text +"</ul>" 
    else :
         #We found 66 sites in location $streetName /$cityName/ $state /$zipcode/ $phoneNo
         text1 ="I found {} ".format(i)
    return i,siteId,text1,siteListUi


def getSiteSearchList(siteList):
    
    
    outputText=[]
    for site in siteList:
        
        "userinput:\"site id 846777\", key1: \"SiteId: 846777  Name: BO TEST SITE Brand: Gilbarco\", key2 :\" Address: 147 MAIN STREET, GREENSBORO, NC\",key3:\"Phone:3432423\",key4:\"This site is showing reported issues\""
        #record="userinput:\"site id {}\",key1: \"SiteID: {}    Name: {}  Brand: {}\", key2 :\" Address: {}, {}, {}, {}\",key3:\"Phone: {}\"".format(site[0],site[0],site[1].upper(),site[2].upper(),site[3].upper(),site[4],site[5],site[6],site[7])    
        userinput="site id {}".format(site[0])
        key1="SiteID: {}    Name: {}  Brand: {}".format(site[0],site[1].upper(),site[2].upper())
        key2="Address: {}, {}, {}, {}".format(site[3].upper(),site[4],site[5],site[6])
        key3="Phone: {}".format(site[7])
        record={'userinput':userinput,'key1':key1,'key2':key2,'key3':key3 }   
        if site[10]>0:
            key4="This site is showing reported issues" 
            record['key4']=key4
        
        print("record : ",record) 
        outputText.append(record)
    return  outputText       


#print(getSiteDetailsSearchBy(['NC','IN'],['GREENSBORO'],'','','','','',True))
   
def processForCitySearch(city_name):
    
    cityList=dbutil.getSiteListByCity(city_name)
    outputText=[]
    i=len(cityList)
   # if i==0:
   #   return "City Name Not Found!!"
    if i>4:
       cityList=cityList[0:4] 
    print('Record Count: ',i)
    info=" We found  {} sites. <ul> ".format(i)
    #header="<tr> <th>{}</th> <th>{}</th><th>{}</th> <th> {}</th> </tr>".format('Site ID','Site Name','Brand','Street Name')    
    outputText.append(info)    
   # outputText.append(header)
    for city in cityList:
        #record="<tr><td>{}</td> <td> {}</td> <td>{}</td> <td> {} </td> </tr>".format(city[0],city[1],city[2],city[3])
       record="<li>Site ID:  {}  Site Name: {} Brand: {} Street Name {} </li>".format(city[0],city[1],city[2],city[3])    
       outputText.append(record)
       text= ''.join(outputText)
    text1 = text +"</ul>" + "\n Please enter the street name to narrow down the search!!"
    
    return text1

def processForCityStreetSearch(city_name,street_name):
    
   cityList=dbutil.getSiteListByCityStreetName(city_name,street_name)
   #i=len(cityList)
   site_id=cityList[0][0]
   print("....site_id...",site_id)
   text=processForValidSiteId(site_id)
   return text

def processForOneCity(city_name):
    cityList=dbutil.getSiteListByCity(city_name)
    i=len(cityList)
    if i==1:
        street_name=  cityList[0][3]
        print("street_name: ",street_name)
        text=  processForCityStreetSearch(city_name,street_name)
    return text
 
def processForfuelingPosition(site_id,fuelingPosition):
    dataList=dbutil.getFuelingPositionDetails(site_id,fuelingPosition)
    isOffline,text=processFuelData(dataList)
    return len(dataList),isOffline,text

#processForfuelingPosition(155881,5)


def processForDispNumber(site_id,dispNo):
    deviceid=dbutil.getDispDeviceId(site_id,dispNo)
    print(deviceid[0][0])
    if len(deviceid) >0 :
        device=deviceid[0][0]
        dataList=dbutil.getDispDetails(site_id,device)
        print("dataList :",dataList)
        isOffline,text=processFuelData(dataList)
    return len(dataList),isOffline,text
    

#processForDispNumber(155881,'2-2')

def processFuelData(dataList):
    isOffline=False    
    outputText=[]    
    i=len(dataList)
    if i==0:
        return isOffline,"No Record Found for today"
    info=" We found  {} issues. <ul> ".format(i)
    #header="<tr> <th>{}</th> <th>{}</th><th>{}</th> <th> {}</th> </tr>".format('Site ID','Site Name','Brand','Street Name')    
    outputText.append(info)    
   # outputText.append(header)
    for data in dataList:
        #record="<tr><td>{}</td> <td> {}</td> <td>{}</td> <td> {} </td> </tr>".format(city[0],city[1],city[2],city[3])
       if data[4] =='offline' or data[4] =='extended-offline' :
            isOffline=True
            print(".......isOffline.............",isOffline)
       record="<li>Fueling Position:  {}  KPI: {}  State: {} Count: {} </li>".format(data[2],data[4],data[5],data[6])    
       outputText.append(record)
    text= ''.join(outputText)
    return isOffline,text


def findOtherOfflineFuelData(site_id):
    dataList=dbutil.getPosOffline(site_id)
   # dataList.sort(key = itemgetter(3,4,5))
   # groups = groupby(dataList, itemgetter(3,4,5))
   # print("groups : ",groups)
    fuelingPosList=[]
    dispensorDeviceList=[]
    isFuelingPosUnique=False
    isDispensorDeviceidUnique=False
    outputText=[]
    phoneNo=dataList[0][6]
    print ("phoneNo : ",phoneNo)
    #for (key, data) in groups:
       # print("key: ",key," group Data: ",dataList)
   #     dataList= list(data)
    for data in dataList:
       fuelingPosList.append(data[2])
       dispensorDeviceList.append(data[1])
       record="<li>Fueling Position:  {}  KPI: {} State: {} Count: {} </li>".format(data[2],data[4],data[5],data[6])    
       outputText.append(record)
    tempSet=set(fuelingPosList)
    tempDevSet=set(dispensorDeviceList)
    print("tempSet  :",tempSet)
    print("tempDevSet  :",tempDevSet)
    text= ''.join(outputText)
    if len(tempSet) ==1:
        isFuelingPosUnique=True
        print("isFuelingPosUnique :",isFuelingPosUnique)
    if len(tempDevSet)  ==1:
        isDispensorDeviceidUnique=True
        print("isDispensorDeviceidUnique :",isDispensorDeviceidUnique)
    if isFuelingPosUnique :
        text ="Only this fueling position is not communicating with the POS. The recommendation is to perform a Warm Restart on this position. Would you like me to perform a Warm Restart on this fueling position.\n" +text
        #return True,text
    elif isDispensorDeviceidUnique :
        text ="This dispenser’s fueling positions are not communicating with the POS. The recommendation is to perform a Warm Restart on both fueling positions. Would you like me to perform a Warm Restart on both fueling position?\n"+text
        #return False,text
    else :
        text="We have identified that multiple fueling positions at the site are not communicating with the POS. The recommendation is to connect the site to have the POS or FCC restarted." +text #The site phone number is "+str(phoneNo)+"\n
        #return False,text
    return  isFuelingPosUnique,text
    
#print(findOtherOfflineFuelData(155881))

def findKpiAndStateOpt1(siteid,deviceid):
    
    kpiList=dbutil.getDispenserIssueOpt1(siteid,deviceid)
    text=getTextForL2(kpiList)
    return text


def findKpiAndStateOpt2(siteid,dispDeviceid):
    
    kpiList=dbutil.getDispenserIssueOpt2(siteid,dispDeviceid)
    text=getTextForL2(kpiList)
    return text

def findKpiAndStateOpt3(siteid,dispenser_position):
    
    disDeviceList=dbutil.getDispenserIssueOpt3(siteid,dispenser_position)
    if len(disDeviceList) >0 :
       device_id=disDeviceList[0][0]
       print("Dispenser DeviceId: ",device_id)
       kpiList=dbutil.getDispenserIssueOpt33(siteid,device_id)
       text=getTextForL2(kpiList)
    return text
 
def processL3DrillDown(fueling_position,device_id):
    
    disDeviceList=dbutil.getDispenserIssueL3(fueling_position,device_id)
    #if len(disDeviceList) >0 :
    text=getTextForL2(disDeviceList)
    return text  
   
def getTextForL2(kpiList):
    if len(kpiList) ==0:
        return "No Record Found"
    outputText=[]
    text=''
    #i=len(kpiList)
    for kpi in kpiList:
        #record="<tr><td>{}</td> <td> {}</td> <td>{}</td> <td> {} </td> </tr>".format(city[0],city[1],city[2],city[3])
       record="<li>The device {} in fueling position {} is showing kpi : {}, state: {}, possible solution: </li>".format(kpi[0],kpi[1],kpi[2],kpi[3])    
       outputText.append(record)
       text= ''.join(outputText)
    return text

def getSiteListTable(siteList):
    siteListTemp=[]
    #i=  len(siteList)  
    title="Similar SiteID " 
    siteListTemp.append(title)
    heading="<tr><td>{}</td> <td> {}</td> <td>{}</td> <td> {} </td> <td> {} </td> </tr>".format('SiteID','Site Name','Brand','Street Name','Phone No')
    siteListTemp.append(heading)    
    for site in siteList:
      record="<tr><td>{}</td> <td> {}</td> <td>{}</td> <td> {} </td> <td> {} </td> </tr>".format(site[0],site[1],site[2],site[3],site[4])
      siteListTemp.append(record)
      site_list_table= ''.join(siteListTemp)
      
    return site_list_table



def getStringFromList(listData):
    text=""
    i=0
    for s in listData:
        
        if i==len(listData)-1:
            text= text+"\'"+s.lower()+"\'" 
        else:
            text= text+"\'"+s.lower()+"\'"+"," 
        i=i+1
    print (text)
    return text

#def getResultObj(resultType,title,data):
    
#   resultObj=[]
 #  resultType['type']=resultType
#   title['title']=title



#i,siteId,text1,siteListUi =getSiteDetailsSearchBy(['NC','IN'],['GREENSBORO'],'','','','','',True)

#result={'type':'list','title':'Site Search','data':siteListUi}

#print(result)

#state= ['IN','NC','PC']
#getStringFromList(state)

#getSimilarSiteList(99)

#print(findKpiAndStateOpt1(195452,12800))

#print(findKpiAndStateOpt2(195452,12799))

#print(findKpiAndStateOpt3(195452,7))

#processForOneCity('SOUTH BURLINGTON')  
    
#processForCityStreetSearch('SOUTH BURLINGTON', '110 KENNEDY DRIVE')


#print(processForValidSiteId('118482'))

#print(processForCitySearch('GREENSBORO')) //GREENSBORO

#'SALISBURY', '1105 PEELER ROAD'
#state_name,city_name, street_name,brand_name,postal_code,phone,site_name,isCityAndStreet

#getSiteDetailsSearchBy(['NC','IN'],['GREENSBORO'],'','','','','',True)