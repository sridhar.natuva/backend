# -*- coding: utf-8 -*-
import yaml
with open("config.yml", 'r') as ymlfile:
    cfg = yaml.load(ymlfile)

class ConfigReader:

    def __init__(self):

        with open("config.yml", 'r') as f:
         self.cfg = yaml.safe_load(f)
         # redshift parameters
         self.redshift = cfg['redshift']
         self.assistant = cfg['assistant']
         self.discovery = cfg['discovery']
   
    def getDbConfig(self):
        return self.redshift
        
    def getAssistantServiceConfig(self):
        return self.assistant
        
    def getRedshiftQuries(self):
        return self.redshift_quries
   
    def getDiscoveryServiceConfig(self):
        return self.discovery